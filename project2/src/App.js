
import React,{useEffect , useState} from 'react';
import { BrowserRouter as Router, Switch , Route} from 'react-router-dom';
import './styles/themes/default/theme.scss';  
import {withRouter} from 'react-router';

import Homepage from './pages/homepage';
import Technology from './pages/technology';
import Culture from './pages/culture';
import Automotive from './pages/automotive';
import Programming from './pages/programming';
import ProgrammingQuestion from './pages/programmingQuestion';



function App() {

  return (
    <Router>
          <Switch>
            <Route  exact path='/' component={Homepage}/>          
            <Route  path='/technology' component={Technology}/>
            <Route  path='/culture' component={Culture}/>
            <Route  path='/automotive' component={Automotive}/>
            <Route  path='/programming' component={Programming}/>
            <Route  path='/programmingQuestion' component={ProgrammingQuestion}/>
            <Route  path='/history' component={History}/>
          </Switch>
    </Router>
  );
}

export default App;