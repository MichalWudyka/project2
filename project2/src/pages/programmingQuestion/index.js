import React,{useEffect , useState} from 'react';

import Left from '../../components/Left';
import Right from '../../components/Right';
import Navigation from '../../components/Navigation';
import HeaderQuestion from '../../components/HeaderQuestion';
import ProgrammingQuiz from '../../components/ProgrammingQuiz';

const  ProgrammingQuestion= () => {

return(
      <div className="programmingQuestion__main-wrapper">
            <Left/> 
            <div className="content__main-wrapper col-8" >
                  <Navigation/>
                  <HeaderQuestion/>
                  <ProgrammingQuiz/>
            </div>
            <Right/> 
      </div>

  )
};


export default ProgrammingQuestion;