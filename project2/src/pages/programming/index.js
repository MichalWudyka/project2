import React,{useEffect , useState} from 'react';

import Left from '../../components/Left';
import Right from '../../components/Right';
import Navigation from '../../components/Navigation';
import Header from '../../components/Header';
import ProgrammingCategory from '../../components/programmingCategory';

const Programming = () => {

return(
      <div className="Programming__main-wrapper">
            <Left/> 
            <div className="content__main-wrapper col-8" >
                  <Navigation/>
                  <Header/>
                  <ProgrammingCategory />
            </div>
            <Right/> 
      </div>
  )
};


export default Programming;