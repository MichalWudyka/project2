import React from 'react';
import { BrowserRouter as Router, Switch , Route } from 'react-router-dom';

import Left from '../../components/Left';
import Right from '../../components/Right';
import Navigation from '../../components/Navigation';
import Header from '../../components/Header';
import Category from '../../components/Category';


const Homepage = () => {

return(
      <div className="homePage__main-wrapper" id="BG11">
            <Left/> 
            <div className="content__main-wrapper col-8" >
                  <Navigation/>
                  <Header/>
                  <Category/>
            </div>
            <Right/> 
      </div>
      )
};

export default Homepage;


