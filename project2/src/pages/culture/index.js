import React from 'react';
import { BrowserRouter as Router, Switch , Route } from 'react-router-dom';

import ProgrammingCategory from '../../components/programmingCategory';


const ProgrammingPage = () => {

return(

      <Router>
        <ProgrammingCategory />
     </Router>

  )
};

export default ProgrammingPage;


