import React,{useEffect , useState} from 'react';
import { BrowserRouter as Router, Switch , Route } from 'react-router-dom';
import {withRouter} from 'react-router';
import ProgrammingCategory from '../../components/programmingCategory';


import Left from '../../components/Left';
import Right from '../../components/Right';
import Navigation from '../../components/Navigation';
import Header from '../../components/Header';
import Category from '../../components/Category';


const Technology = () => {


return(
      <div className="Technology__main-wrapper">
            <Left/> 
            <div className="content__main-wrapper col-8" >
                  <Navigation/>
                  <Header/>
                  
                 
            </div>
            <Right/> 
      </div>
  )
};

export default Technology;
