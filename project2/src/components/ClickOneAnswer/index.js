import React, {useState,useEffect} from 'react';


const ClickOneAnswer = props => {


const [question, setQuestion] = useState();
const [choices, setChoices] = useState([]);
const [userResult, setUserResult] = useState("");

useEffect(()=>{
	setQuestion(props.question);
	setChoices(props.choices);
},[props])


	return(
	<>
		<div>
			<div className='programming__quiz-question'>
				<span> {question} </span>
			</div>
		</div>
		<div className='programming__quiz-answer'>
			{choices.map((answerOption, index) => (
				<button onClick={ () => props.handleAnswerButtonClick(choices[index].isCorrect)}>{answerOption.answerText}</button>
			))}
		</div>
	</>
	);
}
				
export default ClickOneAnswer;


// import React from "react";
// import Child from "./Child";

// const Parent = ({ handleClick }) => (
//   <div>
//     <Child
//       name="First Item"
//       handleClick={() => handleClick("First Item is Clicked!")}
//     />
//     <Child
//       name="Second Item"
//       handleClick={() => handleClick("Second Item is Clicked!")}
//     />
//     <Child
//       name="Third Item"
//       handleClick={() => handleClick("Third Item is Clicked!")}
//     />
//   </div>
// );

// export default Parent;