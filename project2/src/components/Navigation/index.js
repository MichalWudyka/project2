import React from 'react';
import {Link} from 'react-router-dom';


const Navigation = () => {

	return (
		<div className="navigation__main-wrapper">
			<div className="navigation__inner-wrapper">
			<ul className="horizontal-list"> 
				<li>
					<Link className="link" target="_blank" to={{ pathname: "https://easy-code.io/"}} >Easy Code</Link>
				</li>
				<li>
					<Link className="link" target="_blank" to={{ pathname: "https://easy-code.io/#home__shop"}}>Oferta</Link>
				</li>
				<li>
					<Link className="link" target="_blank" to={{ pathname: "https://easy-code.io/#carousel"}}>Tematyka kursu</Link>
				</li>
				<li>
					<Link className="link" target="_blank" to={{ pathname: "https://easy-code.io/platform"}}>Platforma</Link>
				</li>
				<li>
					<Link className="link" to="/">Quiz</Link>
				</li>
			</ul> 
		</div>
		</div>
	)
}


export default Navigation;
