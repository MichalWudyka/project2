import React, {useState,useEffect} from 'react';
import {Link} from 'react-router-dom';

import culture from '../../assets/i_culture.png'; 
import history from '../../assets/i_history.png'; 
import moto from '../../assets/i_moto.png'; 
import program from '../../assets/i_program.png'; 
import technology from '../../assets/i_technology.png'; 
import backG from '../../assets/category_bg.png'; 
import backGH from '../../assets/category_bg_hv.png'; 

import styled from 'styled-components';



const MyRectangle = styled.div`
	width: ${props => props.width +'px'};
	height: ${props => props.height +'px'};
	background: url(${backG}) no-repeat center center ; 
  display: block;
  align-content: space-between;
 	cursor:pointer;
  position: relative;
  background-repeat: no-repeat;
  background-size: contain;
   &:hover {
    background: url(${backGH}) no-repeat center center ; 
    display: block;
    position: relative;
    background-repeat: no-repeat;
    background-size: contain;
    cursor:pointer;
  }
`;

const Category = () => {

let  width;

const [newSize, addNewSize] = useState('');

  useEffect(() => {
  	width = document.getElementById("category__main-wrapper").getBoundingClientRect().width;
    	addNewSize((width/5)-4)
     	console.log(width)
    window.onresize = () => {
    	width = document.getElementById("category__main-wrapper").getBoundingClientRect().width;
    	addNewSize((width/5)-4)
     	console.log(width)
    }
  });

	return (
		<div className="category__main-wrapper" id="category__main-wrapper">
				<Link className="category__buttons-wrapper" to="/technology" >
				<MyRectangle height={newSize}  width={newSize}>
					<div className="img-possition">
						<img src={culture}  alt="Category_Logo"/>
					</div>
					<hr className="category-line" />
					<span  className="category-title" >TECHNOLOGIA</span>
				</MyRectangle>
			</Link> 	
	
			<Link to='/culture' className="category__buttons-wrapper">
				<MyRectangle height={newSize}  width={newSize}>
					<div className="img-possition">
						<img src={history}  alt="Category_Logo"/>
					</div>
					<hr className="category-line" />
					<span className="category-title">KULTURA</span>
				</MyRectangle>
			</Link> 

			<Link to='/automotive' className="category__buttons-wrapper">
				<MyRectangle height={newSize}  width={newSize}>
					<div className="img-possition">
						<img src={moto}  alt="Category_Logo"/>
					</div>
					<hr className="category-line" />
					<span className="category-title" >MOTORYZACJA</span>
				</MyRectangle>
			</Link> 

			<Link to='/programming' className="category__buttons-wrapper">
				<MyRectangle height={newSize}  width={newSize}>
					<div className="img-possition">
						<img src={program}  alt="Category_Logo"/>
					</div>
					<hr className="category-line" />
					<span className="category-title" >PROGRAMOWANIE</span>
				</MyRectangle>
			</Link> 

			<Link to='/history' className="category__buttons-wrapper">
				<MyRectangle height={newSize}  width={newSize}>
					<div className="img-possition">
						<img src={technology}  alt="Category_Logo"/>
					</div>
					<hr className="category-line" />
					<span className="category-title">HISTORIA</span>
				</MyRectangle>
			</Link> 

</div>
	)
}



export default Category;





