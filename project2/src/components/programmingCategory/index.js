import React from 'react';
import {Link} from 'react-router-dom';

import program from '../../assets/i_program.png'; 
import  arrow_play from '../../assets/arrow_play.png'; 


const ProgrammingCategory = () => {


	return (
		<>
			<div className="selectedProgramming__main-wrapper">
				<div className="selectedProgramming__buttons-wrapper">
					<img src={program}  alt="Category_Logo"/>
				</div>
				<hr className="selectedProgramming-line" />
				<span className="selectedProgramming-title" >PROGRAMOWANIE</span>	
			</div>

			<Link className="selectedProgramming-button" to="/programmingQuestion" >
				<span>ROZPOCZNIJ </span>
				<img src={arrow_play}  alt="Category_Logo"/>
			</Link>
		</>
	)
}



export default ProgrammingCategory;
