import React, { useEffect, useState } from 'react';

import logo from '../../assets/QUIZ_LOGO.png'; 
import x from '../../assets/x.png'; 
import back from '../../assets/back.png';

const Header = () => {

let url = window.location.pathname.split('/').pop();

const [title, setTitle] = useState('10 PYTAŃ / 5 KATEGORII');

useEffect(() => {
	console.log(url)
	if (url==='programming'){
		setTitle('WYBRANA KATEGORIA:')
	}
}, [url]);

	return (
		<>
			<div className="header__content-wrapper">
				<div className="header__icons-wrapper">
					<div className="header__icons-wrapper-icons"> 
						<img src={back} alt="<"/> <img src={x} alt="x"/>
					</div>
				</div>
				<div className="header__logo-wrapper">
					<img src={logo} alt="Logo"/>
				</div>	
				<div className="header__info-wrapper">
					<div className="header__info-text">
					<span >{title} </span>
					</div>
				</div>
			</div>	
		</>
	)
}


export default Header;

    
  

  