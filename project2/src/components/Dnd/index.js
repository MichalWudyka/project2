import React, {useState,useEffect} from 'react';
import { DragDropContext, Draggable, Droppable } from "react-beautiful-dnd";


const reorder = (list,startIndex, endIndex) => {
	const result = Array.from(list);
	const [removed] = result.splice(startIndex, 1);
	result.splice(endIndex, 0 , removed)
	return result;
} ;

const Choice = ({ choice, index }) => {
	return( 
		<Draggable draggableId={'a'+ index} index={index}>
		{provided => (
			<div 
				ref={provided.innerRef}
				{...provided.draggableProps}
				{...provided.dragHandleProps}
			>
			{choice}
			</div>
			)}
		</Draggable>
	);
};

const QuestionList = React.memo(function QuestionList ({ choices }){
	return choices.map((choice, index) =>(
		<Choice choice={choice} index={index} key={index} /> 
 		) );
});


const Dnd = props => {

console.log({props});

const [question, setQuestion] = useState();
const [choices, setChoices] = useState([]);
const [userResult, setUserResult] = useState("");

useEffect(()=>{
	setQuestion(props.question);
	setChoices(props.choices);
},[props])

const check = () =>{
	JSON.stringify(choices) === JSON.stringify(props.correct) ?
	// setUserResult("correct");
	props.handleAnswerButtonClick(true)
	:
	// setUserResult("incorrect");
	props.handleAnswerButtonClick(false)
} 

const onDragEnd = result => {
	if(!result.destination){
		return;
	}
	if (result.destination.index === result.source.index){
		return;
	}

	const newChoices = reorder(
		choices,
		result.source.index,
		result.destination.index
	);
	setChoices(newChoices);
}

	return(
<>
		<h2>{question}</h2>
		<DragDropContext onDragEnd = {onDragEnd}>
		

		<div className={`choices_wrapper ${userResult}`}>
			<Droppable droppableId="list" > 
				{provided => (
					<div ref={provided.innerRef} {...provided.droppableProps}>
						<QuestionList choices={choices}/>
						{provided.placeholder}
					</div>
				)}
			</Droppable>
			</div> 


			<button onClick={()=>check()}>Sprawdz</button>
		</DragDropContext>
		</>
	);
}
				
export default Dnd;

	// <div>
	// 		<div>
	// 			<div className='programming__quiz-question'>
	// 				<span> {questions[currentQuestion].questionText} TOOO TYY</span>
	// 			</div>
	// 		</div>
	// 		<div className='programming__quiz-answer'>
	// 			{questions[currentQuestion].answerOptions.map((answerOption, index) => (
	// 				<button onClick={()=> handleAnswerButtonClick(answerOption.isCorrect)}>{answerOption.answerText}</button>
	// 			))}
	// 		</div>
	// 	</div>

