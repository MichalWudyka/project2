import React, { useEffect, useState } from 'react';

import logo from '../../assets/QUIZ_LOGO.png'; 
import x from '../../assets/x.png'; 
import back from '../../assets/back.png';


import {Link} from 'react-router-dom';


const Header = () => {


	return (
		<>
			<div className="header__content-wrapper">
				<div className="header__icons-wrapper">
					<div className="header__icons-wrapper-icons"> 
						<img src={back} alt="<"/> <img src={x} alt="x"/>
					</div>
				</div>
				<div className="header__logo-wrapper">
					<img src={logo} alt="Logo"/>
				</div>	
			</div>	
		</>
	)
}


export default Header;

    
  


