export const Questions = [
	{
    	id: 'q1',
    	question:"what's  9 + 10:",
    	choices: [
			{answerText: '19', isCorrect: true},
			{answerText: '21 ', isCorrect: false},
			{answerText: '12', isCorrect: false},
			{answerText: '22 ', isCorrect: false},
			{answerText: '11', isCorrect: false},
		],
		questionDnd: false,
    },
    {	
    	id: 'q2',
		question: 'Co oznacza skrót CSS:',
		choices: [
			{answerText: 'Cascading Style Sheets', isCorrect: true},
			{answerText: 'Case Style Sheets', isCorrect: false},
			{answerText: 'Counter Strike Style', isCorrect: false},
			{answerText: 'Cross Style Sheets', isCorrect: false},
			{answerText: 'Cascading S-Style', isCorrect: false},
		],
		questionDnd: false,
	},
	{	
		id: 'q3',
		question: 'Który, rodzaj wprowadzenia styli css jest najczęściej stosowany ?',
		choices: [
			{answerText: 'Inline', isCorrect: false},
			{answerText: 'Internal', isCorrect: false},
			{answerText: 'External', isCorrect: true},
			{answerText: 'Inline, Internal ', isCorrect: false},
			{answerText: 'Internal, External ', isCorrect: false},
		],
		questionDnd: false,
	},
	{	
		id: 'q4',
		question: 'Która z pozycji pozwoli ustawić element względem parenta? ',
		choices: [
			{answerText: 'Absolute', isCorrect: true},
			{answerText: 'Relative', isCorrect: false},
			{answerText: 'Fixed', isCorrect: false},
			{answerText: 'Static', isCorrect: false},
			{answerText: 'Unset', isCorrect: false},
		],
		questionDnd: false,
	},
    {
    	id: 'q5',
    	question:"",
    	choices: ["1","2","3"],
    	correct: ["3","2","1"],
    	questionDnd: true,
    },
    {
    	id: 'q6',
    	question:"uporządkuj liczby",
    	choices: ["1","2","3"],
    	correct: ["3","2","1"],
    	questionDnd: true,
    }
];
			