import React, {useState,useEffect} from 'react';

import Dnd from '../Dnd'; 
import ClickOneAnswer from '../ClickOneAnswer'; 
import * as listItem from './programmingQuestions'; 


const questions = [...listItem.Questions];

const ProgrammingQuiz = () => {

const [title, setTitle] = useState(' SELECT THE CORRECT ANSWER');
const [currentQuestion, setCurrentQuestion] = useState(0);
const [showScore, setShowScore] = useState(false);
const [score, setScore] = useState(0);

let questionType = questions[currentQuestion].questionDnd; 

const handleAnswerButtonClick = (isCorrect) => {
	if (isCorrect === true) {
		setScore(score + 1);
		console.log(score+1);
	}

	const nextQuestion = currentQuestion + 1 ; 
	if (nextQuestion < questions.length) {
		setCurrentQuestion(nextQuestion);
	} else {
		setShowScore(true);
	}
};
// console.log(questions[currentQuestion].choices)

return (
	<>
		<div className="header__info-wrapper">
			<div className="header__info-space"/>
				<div className="header__info-textQuestion ">
					<span >{title} </span>
				</div>
				<div className="header__info-question"> 
					<span>{currentQuestion + 1}/  <span  className="pinkTextColor" >   {questions.length} </span></span> 
				</div>
			</div>
		
			{showScore ? 
				<div className='score-section'>
					You scored {score} out of {questions.length}
				</div>
			: questionType ?
				<Dnd handleAnswerButtonClick={handleAnswerButtonClick}  {...questions[currentQuestion]}/>				
				:
				<ClickOneAnswer handleAnswerButtonClick={handleAnswerButtonClick}   {...questions[currentQuestion]}/>
			}
	</>
	)
}



export default ProgrammingQuiz;

// <div>
	// 		<div>
	// 			<div className='programming__quiz-question'>
	// 				<span> {questions[currentQuestion].questionText} TOOO TYY</span>
	// 			</div>
	// 		</div>
	// 		<div className='programming__quiz-answer'>
	// 			{questions[currentQuestion].answerOptions.map((answerOption, index) => (
	// 				<button onClick={()=> handleAnswerButtonClick(answerOption.isCorrect)}>{answerOption.answerText}</button>
	// 			))}
	// 		</div>
	// 	</div>

	// let url = window.location.pathname.split('/').pop();

// useEffect(() => {
// 	console.log(url)
// 	if (url==='programming'){
// 		setTitle('WYBRANA KATEGORIA:')
// 	}

// }, [url]);